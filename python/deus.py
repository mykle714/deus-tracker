import requests
from bs4 import BeautifulSoup
import json
import re
from utility import notify
import datetime
from types import SimpleNamespace
import time

# optimus schema:	["tradeID", "symbol", "longShort", "entryPrice", "exitPrice", "dollarGain", "percentGain", "entryDate", "exitDate"]
# shortus/TVIX:		["tradeID", "symbol", "longShort", "quantity", "entryPrice", "exitPrice", "entryTotal", "exitTotal", "dollarGain", "percentGain", "entryDate", "exitDate"]
def scrape(deusEndpoint, openTrades=True, deusTableFile=""):
	if deusEndpoint == "https://deustrader.com/optimus_mixus4.html":
		schema = ["tradeID", "symbol", "longShort", "entryPrice", "exitPrice", "dollarGain", "percentGain", "entryDate", "exitDate"]
	elif deusEndpoint == "https://deustrader.com/plutus_shortus.html":
		schema = ["tradeID", "symbol", "longShort", "quantity", "entryPrice", "exitPrice", "entryTotal", "exitTotal", "dollarGain", "percentGain", "entryDate", "exitDate"]
	else:
		notify("unknown deus URL")
		return {}

	resp = requests.get(deusEndpoint)
	soup = BeautifulSoup(resp.content, 'html.parser')

	table = {}

	# for every <tr> tag on the page
	for i in soup.find_all("tr"):
		entry = list(i.find_all("td"))
		# remove the <td> from the line
		entry = list(map(lambda x: re.sub("\</?td\>","",str(x)), entry))

		if len(entry) == 0:
			continue
		
		if openTrades and entry[-1] == "(trade open)":
			table[entry[0]] = {}

			for k in enumerate(schema):
				table[entry[0]][k[1]] = entry[k[0]]
		elif not openTrades:
			table[entry[0]] = {}

			for k in enumerate(schema):
				table[entry[0]][k[1]] = entry[k[0]]

	if len(table.keys()) == 0:
		notify("No entries seen from deus")
		return table

	# remove all trades that aren't open
	# if openTrades: table = findOpenTrades(table)

	if len(table.keys()) == 0: notify("No open trades from deus")

	if deusTableFile != "":
		with open(deusTableFile, "w+") as f:
			if deusTableFile[-5:] == ".json":
				json.dump(table,f)
			elif deusTableFile[-4:] == ".txt":
				ret = ""
				for i in table.keys():
					for j in table[i].values():
						ret += str(j)
						ret += "\t"
					ret += "\n"

				f.write(ret)

	return table

# trims the table provided to only allow open trades
def findOpenTrades(table):
	openTrade = "(trade open)"
	ret = {}
	notify = True
	for i in table.keys():
		if table[i]["exitDate"] == openTrade:
			ret[i] = table[i]

		if table[i]["exitDate"] == openTrade: notify = False
	
	if notify: notify("Deus showing no open trades")
	
	return ret

# parses the date showed on deus's table in datetime format
def parseDate(s):
	return datetime.datetime.strptime(s, "%Y-%m-%d %H:%M EST")

# parses the price shown on deus's table to float
def parsePrice(s):
	match = re.search("\d+\.\d\d", s)
	return float(match.group(0)) if match else None

def stringifyTable(deusTableFile):
	with open(deusTableFile, "r") as f:
		table = json.load(f)

	with open("./prettyTable.txt", "w+") as f: pass

	with open("./prettyTable.txt", "a") as f:
		temp = ""
		for i in table.keys():
			print(i)
			for j in table[i].keys():
				temp += table[i][j]
				temp += "\t"

			temp += "\n"

		f.write(temp)

def getDeusPositions(t, options):
	# Then pull all open trades from deus's table
	try:
		deusTable = scrape(options["endpoint"]) # 1.5 seconds
	except:
		notify("error with scraping deus table")
		return
	if len(deusTable.keys()) == 0: notify("deus table empty")

	# deusPositions is the dictionary of what our position SHOULD look like
	deusPositions = {}
	# deusTableReduced is just the deus table in list form and less columns
	deusTableReduced = []
	if options["endpoint"] == "https://deustrader.com/optimus_mixus4.html":
		for i in deusTable.keys():
			symbol = deusTable[i]["symbol"]
			entryPrice = parsePrice(deusTable[i]["entryPrice"])

			# increment the qty for a symbol
			qty = calcQtyOptimus(t, entryPrice, options["targetPrice"], options["maxPrice"])
			if qty > 0:
				deusPositions[symbol] = qty + (deusPositions[symbol] if symbol in deusPositions.keys() else 0)

				deusTableReduced.append( (symbol, qty, entryPrice) )

	elif options["endpoint"] == "https://deustrader.com/plutus_shortus.html":
		for i in deusTable.keys():
			symbol = deusTable[i]["symbol"]
			entryPrice = parsePrice(deusTable[i]["entryPrice"])
			entryTotal = parsePrice(deusTable[i]["entryTotal"])
			
			qty = -calcQtyShortus(entryPrice, entryTotal, options["targetPrice"], options["maxPrice"])
			if qty < 0:
				deusPositions[symbol] = qty + (deusPositions[symbol] if symbol in deusPositions.keys() else 0)
				deusTableReduced.append( (symbol, qty, entryPrice) )
	else:
		notify("unknown deus URL")

	return deusPositions, deusTableReduced

def calcQtyOptimus(t, entryPrice, targetPrice, maxPrice):
	entryPrice	= float(entryPrice)
	targetPrice = float(targetPrice)
	maxPrice	= float(maxPrice)
	if targetPrice == 0: targetPrice = t.getEquity()/100.0

	ret = round(targetPrice/entryPrice) if entryPrice <= maxPrice else 0

	return ret

def calcQtyShortus(entryPrice, entryTotal, targetPrice, maxPrice):
	entryPrice	= float(entryPrice)
	targetPrice = float(targetPrice)
	entryTotal	= float(entryTotal)
	maxPrice	= float(maxPrice)

	ret = round(targetPrice/entryPrice) if entryPrice <= maxPrice else 0
	ret = ret*2 if entryTotal < -1500 else ret

	return ret
