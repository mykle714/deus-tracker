import deus
import alpaca
from types import SimpleNamespace
import json
import utility
import sys
import reporting
import asyncio
import time
import boto3
import datetime
import dateutil

async def main():
    # First load all the paramaters you need
    params = SimpleNamespace(**utility.getParams("params.json"))

    try:
        t = alpaca.Trader(paper=params.paper)
    except:
        utility.notify("error connecting to alpaca")
        return

    optionsList = [
        {
            "endpoint":		params.optimusEndpoint,
            "targetPrice":	params.targetPrice,
            "maxPrice":		params.maxPrice,
            "percent":		params.optimusPercent,
            "go":			params.optimusGo
        },
        {
        	"endpoint":     params.shortusEndpoint,
            "targetPrice":	params.targetPrice,
            "maxPrice":		params.maxPrice,
        	"percent":	    params.shortusPercent,
        	"go":		    params.shortusGo
        }
    ]

    for options in optionsList:
        if options["go"]:
            myPositions = await t.getActualPositions() # 0.3 seconds
            deusPositions = {}
            deusTableReduced = []

            temp1, temp2 = deus.getDeusPositions(t, options) # 1.5 seconds
            deusPositions = utility.addToDict(temp1, deusPositions)
            deusTableReduced += temp2

            await t.makeTrades(myPositions, deusPositions, deusTableReduced, params.tolerance, options["endpoint"])

    t.saveTempBlacklist()

if __name__ != "__main__":
    sys.exit()


now = datetime.datetime.now(datetime.timezone.utc)
from_zone = dateutil.tz.gettz('UTC')
to_zone = dateutil.tz.gettz('America/New_York')
eastern = now.astimezone(to_zone)

if eastern.hour < 16 and (eastern.hour > 9 or (eastern.hour == 9 and eastern.minute >= 30)):
    try:
        asyncio.run(main())
    except ValueError:
        pass
    reporting.main()
else:
    print("past hours")