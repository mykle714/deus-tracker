import utility
import alpaca
from types import SimpleNamespace
import json
import datetime
import dateutil
import pytz
import time

def report(setting, period=None, timeframe=None, paper=True):
	try:
		t = alpaca.Trader(paper=paper)
	except:
		utility.notify("error connecting to alpaca")

	e = float(t.alpaca.get_account().equity)

	if period is not None and timeframe is not None:
		history = t.alpaca.get_portfolio_history(period=period, timeframe=timeframe)
		print(history.base_value)
		print("\t".join([str(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(i))) for i in history.timestamp]))
		print("\t\t\t".join([str(i) for i in history.profit_loss]))
		print("\t\t\t".join([str(i) for i in history.equity]))

	elif setting == "d":
		history = t.alpaca.get_portfolio_history(period="1W", timeframe="1D")
		p = "daily"
		diff = e - history.equity[-2]
		ratio = diff / history.equity[-2]
	elif setting == "w":
		history = t.alpaca.get_portfolio_history(period="1W", timeframe="1D")
		p = "weekly"
		diff = e - history.equity[0]
		ratio = diff / history.equity[0]
	elif setting == "m":
		history = t.alpaca.get_portfolio_history(period="1M", timeframe="1D")
		p = "monthly"
		diff = e - history.equity[0]
		ratio = diff / history.equity[0]
	elif setting == "q":
		history = t.alpaca.get_portfolio_history(period="90D", timeframe="1D")
		p = "quarterly"
		diff = e - history.equity[0]
		ratio = diff / history.equity[0]
	elif setting == "a":
		history = t.alpaca.get_portfolio_history(period="1Y", timeframe="1D")
		p = "yearly"
		diff = e - history.equity[0]
		ratio = diff / history.equity[0]

	print(history.equity)

	plus = "+" if diff > 0 else "-"

	notifyStr = \
"""```diff
%sTotal Equity: $%.2f
%s%s $Gain: $%.2f
%s%s %%Gain: %.2f%%```""" % (plus, e, plus, p, diff, plus, p, ratio)

	utility.notify(notifyStr)

def main(override=False):
	params = SimpleNamespace(**utility.getParams("params.json"))
	paper = params.paper

	now = datetime.datetime.now(datetime.timezone.utc)
	from_zone = dateutil.tz.gettz('UTC')
	to_zone = dateutil.tz.gettz('America/New_York')
	eastern = now.astimezone(to_zone)
	
	if not override:
		if eastern.hour != params.reportHour or eastern.minute != params.reportMinute:
			return

	# utility.notify("current hour is: %d" % eastern.hour)
	# utility.notify("current minute is: %d" % eastern.minute)

	if now.weekday() >= 0 and now.weekday() <=4:
		report(setting="d", paper=paper)

	if now.weekday() == 4:
		report(setting="w", paper=paper)

	compare = datetime.datetime(year=now.year, month=now.month+1, day=1, hour=0, minute=0, second=0, microsecond=0) - datetime.timedelta(days=1)
	if now.day == compare.day:
		report(setting="m", paper=paper)

	compare1 = datetime.datetime(year=now.year  , month=4 , day=1, hour=0, minute=0, second=0, microsecond=0) - datetime.timedelta(days=1)
	compare2 = datetime.datetime(year=now.year  , month=7 , day=1, hour=0, minute=0, second=0, microsecond=0) - datetime.timedelta(days=1)
	compare3 = datetime.datetime(year=now.year  , month=10, day=1, hour=0, minute=0, second=0, microsecond=0) - datetime.timedelta(days=1)
	compare4 = datetime.datetime(year=now.year+1, month=1 , day=1, hour=0, minute=0, second=0, microsecond=0) - datetime.timedelta(days=1)
	if  (now.day == compare1.day and now.month == compare1.month) or \
		(now.day == compare2.day and now.month == compare2.month) or \
		(now.day == compare3.day and now.month == compare3.month) or \
		(now.day == compare4.day and now.month == compare4.month):
		report(setting="q", paper=paper)

	compare = datetime.datetime(year=now.year+1, month=1 , day=1, hour=0, minute=0, second=0, microsecond=0) - datetime.timedelta(days=1)
	if now.day == compare.day and now.month == compare.month:
		report(setting="a", paper=paper)


	