import boto3
import json
from datetime import datetime
import requests
import os

def notify(msg, prepend=True):
	with open("./awsParams.json", "r") as f:
		params = json.load(f)

	if params["notifications"] == "sns":
		client = boto3.client("sns",
			aws_access_key_id=params["AWSAccess"],
			aws_secret_access_key=params["AWSSecret"],
			region_name="us-east-1")

		client.publish(
			PhoneNumber=params["phoneNumber"],
			Message="DeusBot:\n" + msg if prepend else msg)
			
	elif params["notifications"] == "discord":
		webhook = {"content": msg}
		requests.post(params["webhook"], webhook)

	print(msg)

def flipDict(d):
	ret = {}
	for i in d.keys():
		ret[d[i]] = i

	return ret

def matchDeusTable(deusEntry, myOrders):
	for i in myOrders:
		if deusEntry["symbol"] == i.symbol and deusEntry["quantity"] == i.qty:
			return True

	return False

def to2(a):
	return float("%.2f" % (a))

def addToDict(toAdd, addTo):
	for i in toAdd.keys():
		if i in addTo.keys():
			notify("Overlap between dictionaries: " + i)
			continue
		addTo[i] = toAdd[i]

	return addTo

def getParams(paramString):
	with open("./awsParams.json", "r") as f:
		params = json.load(f)

	if params["fromS3"]:
		checkOS = os.environ.get(paramString)
		if checkOS is None:
			s3 = boto3.client("s3",
				aws_access_key_id=params["AWSAccess"],
				aws_secret_access_key=params["AWSSecret"],
				region_name="us-east-1")

			obj = s3.get_object(Bucket="deus-tracker", Key=paramString)
			temp = obj['Body'].read().decode("utf-8")
			os.environ[paramString] = temp
			ret = json.loads(temp)
		else:
			ret = json.loads(checkOS)
	else:
		with open("./" + paramString, "r") as f:
			ret = json.load(f)

	return ret

def saveParams(obj, filename):
	with open("./awsParams.json", "r") as f:
		params = json.load(f)

	if params["fromS3"]:
		putS3(obj, filename)
	else:
		with open("./" + filename, "w") as f:
			json.dump(obj, f)

def putS3(obj, filename):
	with open("./awsParams.json", "r") as f:
		params = json.load(f)

	if params["fromS3"]:
		s3 = boto3.client("s3",
			aws_access_key_id=params["AWSAccess"],
			aws_secret_access_key=params["AWSSecret"],
			region_name="us-east-1")

		obj = s3.put_object(Body=json.dumps(obj), Bucket="deus-tracker", Key=filename)
	else:
		print("not from S3")