import alpaca_trade_api as alpaca
import json
import sys
import datetime
from types import SimpleNamespace
from utility import notify
import utility
from functools import reduce
import requests
import time
import asyncio

# class used to interact with alpaca
class Trader:
	def __init__(self, paper=True):
		self.params = SimpleNamespace(**utility.getParams("params.json"))
		self.permBlacklist = utility.getParams("blacklist.json")
		self.tempBlacklist = utility.getParams("tempBlacklist.json")
		self.blacklist = list(set(self.permBlacklist + self.tempBlacklist))
			
		alpacaTablePath	= self.params.alpacaTablePath
		self.apiKey = self.params.alpacaAPIKeyPaper if paper else self.params.alpacaAPIKeyLive
		self.apiSecret = self.params.alpacaSecretPaper if paper else self.params.alpacaSecretLive
		self.baseURL = self.params.alpacaPaperEndpoint if paper else self.params.alpacaLiveEndpoint
		self.alpaca = alpaca.REST(self.apiKey, self.apiSecret, self.baseURL, api_version='v2')
		self.paper = paper

	async def makeTrades(self, myPositions, deusPositions, deusTableReduced, tolerance, endpoint, portfolioPercent=1.0):
		# allAssets are all the assets between deus and me for iteration purposes
		submitOrderCoroutines = set()
		allAssets = set(list(deusPositions.keys()) + list(myPositions.keys()))
		for symbol in allAssets:
			try:
				# pad missing symbols with 0 quantity
				myQty = myPositions[symbol] if symbol in myPositions.keys() else 0
				deusQty = deusPositions[symbol] if symbol in deusPositions.keys() else 0

				orderType = "market"
				timeInForce = "gtc"
				side = None

				# when buying, also ensure you're getting a similar (by tolerance) price. The reduce statement pulls the last price seen of that symbol for this calculation
				lastDeusPrice = reduce(lambda x,y: y[2] if y[0] == symbol else x, deusTableReduced.__reversed__())
				# convoluted way of saying that if this strategy doesn't have an open trade of this symbol, ignore it, it's from another strategy
				if type(lastDeusPrice) == type( (0,0) ):
					continue

				askPrice = self.alpaca.get_last_quote(symbol).askprice
				if endpoint == "https://deustrader.com/optimus_mixus4.html":
					if deusQty > myQty and askPrice <= tolerance * lastDeusPrice:
						qty = deusQty - myQty
						side = "buy"
					elif myQty > deusQty:
						qty = myQty - deusQty
						side = "sell"
				elif endpoint == "https://deustrader.com/plutus_shortus.html":
					if deusQty < myQty and askPrice >= (2-tolerance) * lastDeusPrice:
						qty = myQty - deusQty
						side = "sell"
					elif myQty < deusQty:
						qty = deusQty - myQty
						side = "buy"

				if side is not None:
					# failing to get an asset likely means alpaca doesnt have it, so we just keep skipping it
					# also assets tell us whether or not theyre tradable in alpaca
					asset = await self.getAsset(symbol)
					if not asset.tradable:
						continue

					# ignore symbols in the blacklist
					if symbol in self.blacklist:
						continue
					
					submitOrderCoroutines.add(asyncio.create_task(self.submitOrder(symbol, qty, side, orderType, timeInForce, portfolioPercent=portfolioPercent))) # 0.4 seconds

			except alpaca.rest.APIError:
				continue
			except requests.exceptions.HTTPError:
				continue
		
		await asyncio.wait(submitOrderCoroutines,return_when=asyncio.FIRST_COMPLETED)
			

	# submit an order to alpaca
	async def submitOrder(self, symbol, qty, side, orderType, timeInForce, limitPrice=None, stopPrice=None,clientOrderID="", portfolioPercent=1.0):
		qty = int(qty)

		task1 = asyncio.create_task(self.getLastQuote(symbol))
		task2 = asyncio.create_task(self.getAsset(symbol))
		done, pending = await asyncio.wait({task1, task2})
		temp = task1.result()
		asset = task2.result()

		askPrice = temp.askprice

		if askPrice == 0:
			self.tempBlacklist.append(symbol)
			return None

		if side == "sell":
			self.tempBlacklist = []
			pos = await self.getPositions()
			short = True
			for i in pos:
				if i.symbol == symbol and int(i.qty) >= qty: short = False
			
			if short and not asset.easy_to_borrow:
				self.tempBlacklist.append(symbol)
				return None
		else:
			getCashTask = asyncio.create_task(self.getCash())
			getEquityTask = asyncio.create_task(self.getEquity())
			done, pending = await asyncio.wait({getCashTask, getEquityTask})
			cash = getCashTask.result()
			equity = getCashTask.result()

			if cash - equity*(1-portfolioPercent) < askPrice*qty:
				notify("```%s\t%.2f\n%s\t%.2f\t%d\n%s\t%s\t%s\t%s```" % ("no cash", cash, symbol, askPrice, qty, orderType, side, timeInForce, clientOrderID))
				self.tempBlacklist.append(symbol)
				return None

		# orderObj = await self.submitOrderAlpaca(symbol, qty, side, orderType, timeInForce, limitPrice, stopPrice, clientOrderID)
		# return orderObj
		try:
			orderObj = await self.submitOrderAlpaca(symbol, qty, side, orderType, timeInForce, limitPrice, stopPrice, clientOrderID)
			if self.params.notifyTrades and orderObj.status == "accepted": notify("submit order success " + symbol)
			# print("Market order of | %s %d %s %s %s %s %s | completed." % (symbol, qty, side, orderType, timeInForce, limitPrice, stopPrice))
			print("submit order success " + symbol)
			return orderObj
		except:
			# print("Market order of | %s %d %s %s %s %s %s | failed. %s" % (symbol, qty, side, orderType, timeInForce, limitPrice, stopPrice, sys.exc_info()[0]))
			# notify("```%s\n%s\t%.2f\t%d\n%s\t%s\t%s\t%s```" % ("trade failed", symbol, askPrice, qty, orderType, side, timeInForce, clientOrderID))
			if self.params.notifyTrades: notify("submit order fail " + symbol)
			print("submit order fail " + symbol)
			self.tempBlacklist.append(symbol)
			return None

	async def submitOrderAlpaca(self, symbol, qty, side, orderType, timeInForce, limitPrice, stopPrice, clientOrderID):
		return self.alpaca.submit_order(symbol, qty, side, orderType, timeInForce, limitPrice, stopPrice, clientOrderID)

	async def getPositions(self):
		return self.alpaca.list_positions()

	async def getLastQuote(self,symbol):
		return self.alpaca.get_last_quote(symbol)
	
	def getRelevantOrders(self):
		# All relevant orders are all the orders that are responsible for our current positions.
		relOrders = []
		# We only care about what quantity we have of each asset, so we will make a new positions dict to populate
		positions = {}
		# first we call our own getPostions() method to get a list of position objects
		for i in self.getPositions():
			# since everything is stored as a string in these objects, we need to cast it to an int.
			positions[i.symbol] = int(i.qty)
		
		# Since alpaca only allows us to pull 500 orders at a time, we need to pull them sequencially.
		# The way to do this is to use timestamps to tell alpaca which set of orders to give us
		# To start, we want the most recent set of orders
		until = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S-04:00")

		# We need to continually pull data until all the positions we have are accounted for.
		# The algorithm we are using treats each symbol separately. For this reason,
		# we need a set of symbols to not be considered.
		completed = set() # {symbol: boolean}
		# The next thing to do is to iterate through the order history in reverse.
		# While doing this, we keep a list of sells and a sum for each symbol.
		sells = {} # {symbol: [client_order_id]}
		# with each buy, we check if there is a matching sell by client order ID,
		# if there is, then we don't add it to the sum, otherwise, add it.
		# When the sum of the sell-less buys reaches the current position, we can stop reading for this symbol.
		sums = {}
		# Keep looping until we've accounted for all the positions.
		while(completed != set(positions.keys())):
			# We start each iteration by pulling the orders with the iteration's timestamp.
			orders = self.getOrders(until)
			for i in orders:
				# If the order is a sell, add it to the list
				if i.side == "sell":
					# client_order_id ends in either 'o' or 'c' for open or close, just want to omit that.
					sells[i.symbol] = [i.client_order_id[:-1]] + (sells[i.symbol] if i.symbol in sells.keys() else [])
				# if the order is a buy, check it's client order id against the list of sells before adding it to the sum.
				elif i.side == "buy" and i.symbol in sells.keys() and i.client_order_id[:-1] not in sells[i.symbol]:
					relOrders.append(i)
					sums[i.symbol] = int(i.qty) + (sums[i.symbol] if i.symbol in sums.keys() else 0)
					# after updating the sum, we need to check if it matches the current position quantity
					if sums[i.symbol] == positions[i.symbol]: completed.add(i.symbol)

			# we need to update the timestamp cap for the next iteration
			until = orders[-1].submitted_at

		return relOrders
	
	async def getOrders(self, status="all", limit=500, until=""):
		return self.alpaca.list_orders(status=status, limit=limit, until=until)

	async def getCash(self):
		return float(self.alpaca.get_account().cash)

	async def getEquity(self):
		return float(self.alpaca.get_account().equity)

	async def getAsset(self,symbol):
		return self.alpaca.get_asset(symbol)

	# gets a specific alpaca order
	async def getOrder(self, orderID):
		orders = self.getOrders()
		for i in orders:
			if orderID == i.id:
				return i._raw

		return None
			
	async def getActualPositions(self):
		# Then get all of your own positions
		alpacaPositions = await self.getPositions()
		if len(alpacaPositions) == 0: utility.notify("0 positions pulled")

		limit = 500
		alpacaOpenOrders = await self.getOrders(status="open", limit=limit)
		if len(alpacaOpenOrders) >= limit: utility.notify("More than 500 open orders pulled")
		# myPositions is the dictionary of what our position ACTUALLY look like plus the open orders representing what it will look like
		myPositions = {}
		# account for all the normal positions
		for i in alpacaPositions:
			myPositions[i.symbol] = int(i.qty)
		
		# account for all the positions about to happen in the open orders
		for i in alpacaOpenOrders:
			# complicated way of adding orders to positions if it's a buy and subtracting if it's a sell
			myPositions[i.symbol] = (myPositions[i.symbol] if i.symbol in myPositions.keys() else 0) + \
									(int(i.qty) if i.side == "buy" else -int(i.qty))

		return myPositions

	def saveTempBlacklist(self):
		utility.putS3(self.tempBlacklist, "tempBlacklist.json")

# determine if your order went through based on the status
def validateOrderStatus(orderStatus):
	if  orderStatus == "rejected"	or \
		orderStatus == "stopped"	or \
		orderStatus == "suspended"	or \
		orderStatus == "canceled"	or \
		orderStatus == "expired":

		return False

	return True

# parse dates that alpaca uses to datetime format
def parseDate(s):
	temp = s.split(".")
	return datetime.datetime.strptime(temp[0], "%Y-%m-%dT%H:%M:%S")

class Processor:
	def __init__(self):
		pass

	def summarize(self, data):
		openn = data[0]['o']
		close = data[-1]['c']
		high = 0
		low = 0
		for i in data:
			if i['h'] > high: high = i['h']
			if i['l'] < low: low = i['l']

		return openn, high, low, close

	def extractValueAlpaca(self, data, value):
		if type(data) == type({}):
			retTime = {}
			ret = {}
			for i in data.keys():
				tempTime = [0]*len(data[i])
				temp = [0]*len(data[i])
				for j in enumerate(data[i]):
					tempTime[j[0]] = j[1]['t']
					if value == "open"  : temp[j[0]] = j[1]['o']
					if value == "high"  : temp[j[0]] = j[1]['h']
					if value == "low"   : temp[j[0]] = j[1]['l']
					if value == "close" : temp[j[0]] = j[1]['c']
					if value == "volume": temp[j[0]] = j[1]['v']

				retTime[i] = tempTime
				ret[i] = temp
		elif type(data) == type([]):
			retTime = [0]*len(data)
			ret = [0]*len(data)
			for j in enumerate(data):
				retTime[j[0]] = j[1]['t']
				if value == "open"  : ret[j[0]] = j[1]['o']
				if value == "high"  : ret[j[0]] = j[1]['h']
				if value == "low"   : ret[j[0]] = j[1]['l']
				if value == "close" : ret[j[0]] = j[1]['c']
				if value == "volume": ret[j[0]] = j[1]['v']
		else:
			print(type(data))

		return ret, retTime

	def transposeDict(self,dictList):
		ret = {}
		for i in dictList[0].keys():
			ret[i] = []
			for j in dictList:
				ret[i].append(j[i])

		return ret

	def convertKeysChar2Str(self,c):
		if c == "o": return "open"
		if c == "h": return "high"
		if c == "l": return "low"
		if c == "c": return "close"
		if c == "v": return "volume"
		if c == "t": return "time"

	def to2(self,a):
		return float("%.2f" % (a))

	def listTo2(self,a):
		return list(map(lambda x: self.to2(x), a))