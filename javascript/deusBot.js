const fs = require('fs');
const utility = require("./utility.js")

const Discord = require('discord.js');
const client = new Discord.Client();

const AWS = require("aws-sdk");
params = JSON.parse(fs.readFileSync("../python/awsParams.json"))
access = params.AWSAccess
secret = params.AWSSecret
const awsCreds = new AWS.Credentials(access, secret)
AWS.config.update({region: "us-east-1"})
const s3 = new AWS.S3({apiVersion: '2006-03-01', credentials: awsCreds})
const bucketName = "deus-tracker"

var channelID = "718249687030824980"
var channel

client.on('ready', () => {
	channel = client.channels.cache.get(channelID);
	channel.send("bot online")
	console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', async message => {
	var msg = message.content
	// console.log("got message")
	if(message.author.bot || message.channel != channelID) {
		return
	}

	msgSplit = msg.split(" ")

	console.log(msgSplit)

	switch(msgSplit[0]) {
		case "blacklist":
			blacklist = await utility.s3Get(s3, "blacklist.json")
			if(msgSplit[1] == "add") {
				for(i = 2; i < msgSplit.length; i++) {
					if(blacklist.filter(x => x == msgSplit[i]).length == 0) {
						blacklist.push(msgSplit[i])
					} else {
						channel.send(msgSplit[i] + " already blacklisted")
					}
					
				}
				utility.s3Put(s3, blacklist, "blacklist.json")
				channel.send("add complete")
			} else if(msgSplit[1] == "remove") {
				for(i = 2; i < msgSplit.length; i++) {
					blacklist = blacklist.filter(x => x != msgSplit[i])
				}
				utility.s3Put(s3, blacklist, "blacklist.json")
				channel.send("removal complete")
			} else if(msgSplit[1] == "print") {
				toPrint = blacklist.toString()
				toPrint = toPrint.length == 0 ? "Empty Blacklist" : toPrint
				channel.send(toPrint)
			} else {
				channel.send("error")
			}
			
			break
		
		case "go":
			params = await utility.s3Get(s3, "params.json")
			if(Object.keys(params).includes(msgSplit[1].toLowerCase() + "Go")) {
				temp = msgSplit[2].toLowerCase() == "on"
				params[msgSplit[1] + "Go"] = temp
				utility.s3Put(s3, params, "params.json")
				channel.send("deus tracker switched " + (temp ? "on" : "off"))
			} else {
				channel.send("No such strategy: " + msgSplit[1].toLowerCase())
			}

			break

		case "view":
			input = msgSplit[1].toLowerCase()
			filename = input.substring(input.length-5) == ".json" ? input : input + ".json"
			try {
				params = await utility.s3Get(s3, filename)
				msg = JSON.stringify(params).replace(/\,/gi, ",\n")
				channel.send(msg,{"code": true})
			} catch(err) {
				channel.send("file not found")
			}
			
			break

		case "notifyTrades":
			params = await utility.s3Get(s3, "params.json")
			if(msgSplit[1] == "on") {
				params.notifyTrades = true
				utility.s3Put(s3, params, "params.json")
				channel.send("Trade notifications turned on")
			} else if(msgSplit[1] == "off") {
				params.notifyTrades = false
				utility.s3Put(s3, params, "params.json")
				channel.send("Trade notifications turned off")
			} else {
				channel.send("please indicate `on` or `off`")
			}

			break
		case "help":
			channel.send("```blacklist *add/remove* *symbol(s)*\ngo *strategy* *on/off*\nnotifyTrades *on/off*```")
			break
	}
})

async function main() {
	auth = await utility.s3Get(s3, "auth.json")
	client.login(auth.token)
}

main()