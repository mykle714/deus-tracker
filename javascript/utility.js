const bucketName = "deus-tracker"

function s3Put(s3, object, fileName) {
	console.log("putting " + fileName)
	var params = {
		Body: JSON.stringify(object), 
		Bucket: bucketName, 
		Key: fileName
	}
	s3.putObject(params, function(err,data) {
		if(err) console.log(err)
		else {
			console.log(fileName + " putten")
		}
	})
}
function s3Get(s3, fileName) {
	console.log("getting " + fileName)
	var params = {
		Bucket: bucketName, 
		Key: fileName
	}

	return new Promise(function(resolve, reject) {
		s3.getObject(params, function(err,data) {
			if(err) reject(err)
			else {
				console.log(fileName + " gotten")
				resolve(JSON.parse(data.Body))
			}
		})
	})
}

module.exports = {
	s3Put: s3Put,
	s3Get: s3Get
}